{-# LANGUAGE ForeignFunctionInterface, EmptyDataDecls #-}

import Foreign
import Foreign.Ptr
import Foreign.C.String
import Foreign.C.Types
import System.IO.Unsafe

#include "window.h"

#def struct container { struct display *display; struct window *window; };

data Display
data Window
data Widget
data Container = Container { display :: ForeignPtr Display, window :: ForeignPtr Window }

instance Storable Container where
    sizeOf _    = #{size struct container}
    alignment _ = #{alignment struct container}
    peek ptr = do
        d_ptr <- #{peek struct container, display} ptr
        w_ptr <- #{peek struct container, window} ptr
        d_fp <- newForeignPtr finalizerFree d_ptr
        w_fp <- newForeignPtr finalizerFree w_ptr
        return (Container d_fp w_fp)
    poke ptr (Container d_fp w_fp) = do
        withForeignPtr d_fp $ \d_ptr -> do
        withForeignPtr w_fp $ \w_ptr -> do
            #{poke struct container, display} ptr d_ptr
            #{poke struct container, window} ptr w_ptr

foreign import ccall unsafe "display_create"
    c_display_create :: CInt -> Ptr CString -> IO (Ptr Display)

display_create = unsafePerformIO $
    alloca $ \argv -> do
        d_ptr <- c_display_create 0 argv
        d_fp  <- newForeignPtr finalizerFree d_ptr
        return d_fp

foreign import ccall unsafe "window_create"
    c_window_create :: Ptr Display -> IO (Ptr Window)

window_create d_fp = unsafePerformIO $ do
    withForeignPtr d_fp $ \d_ptr -> do
        w_ptr <- c_window_create d_ptr
        w_fp <- newForeignPtr finalizerFree w_ptr
        return w_fp

foreign import ccall unsafe "window_frame_create"
    c_window_frame_create :: Ptr Window -> Ptr a -> IO (Ptr Widget)

window_frame_create w_fp c_ptr = unsafePerformIO $ do
    withForeignPtr w_fp $ \w_ptr -> do
        wdg_ptr <- c_window_frame_create w_ptr c_ptr
        wdg_fp <- newForeignPtr finalizerFree wdg_ptr
        return wdg_fp

foreign import ccall unsafe "display_run"
    c_display_run :: Ptr Display -> IO ()

display_run d_fp = unsafePerformIO $ do
    withForeignPtr d_fp $ \d_ptr -> do
        return $ c_display_run d_ptr

foreign import ccall unsafe "widget_schedule_resize"
    c_widget_schedule_resize :: Ptr Widget -> CInt -> CInt -> IO ()

widget_schedule_resize wdg_fp = unsafePerformIO $ do
    withForeignPtr wdg_fp $ \wdg_ptr -> do
        return $ c_widget_schedule_resize wdg_ptr 200 200

main = do
    let d_fp = display_create
    let w_fp = window_create d_fp
    alloca $ \c_ptr -> do
        let c = Container d_fp w_fp
        poke c_ptr c
        let wdg = window_frame_create w_fp c_ptr
        widget_schedule_resize wdg
    display_run d_fp
    return 0

