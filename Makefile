LDLIBS=-lwayland-client -lxkbcommon -lwayland-cursor -lpixman-1 -lcairo -lpng16 -lm

CFLAGS=-I/usr/include/cairo

westondeps = ../weston/protocol/weston_desktop_shell-weston-desktop-shell-protocol.o ../weston/.libs/libtoytoolkit.a

all: myth
.PHONY: all

myth: myth.c $(westondeps)

hsmyth: hsmyth.c $(westondeps)

mythmin: mythmin.c $(westondeps)

hs: hswayland.hs hswayland
.PHONY: hs

hswayland.hs: hswayland.hsc
	hsc2hs $(CFLAGS) $^

hswayland: hswayland.hs $(westondeps)
	ghc --make $(LDLIBS) $^

mythminwayland.hs: mythminwayland.hsc
	hsc2hs $(CFLAGS) $^

mythminwayland: mythminwayland.hs $(westondeps)
	ghc --make $(LDLIBS) $^

