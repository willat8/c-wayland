#include "window.h"

int
main(int argc, char *argv[])
{
    struct window *window;
    struct widget *widget;
    struct display *display;

    display = display_create(&argc, argv);
    if (display == NULL) {
        return -1;
    }

    window = window_create(display);
    widget = window_frame_create(window, NULL);
    widget_schedule_resize(widget, 200, 200);

    display_run(display);

    widget_destroy(widget);
    window_destroy(window);
    display_destroy(display);

    return 0;
}

