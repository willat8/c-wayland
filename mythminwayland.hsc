{-# LANGUAGE ForeignFunctionInterface, EmptyDataDecls #-}

import Foreign
import Foreign.Ptr
import Foreign.C.String
import Foreign.C.Types

#include "window.h"
#include "weston-desktop-shell-client-protocol.h"

#{def struct surface {
    void (*configure)(void *data,
                      struct weston_desktop_shell *desktop_shell,
                      uint32_t edges,
                      struct window *window,
                      int32_t width,
                      int32_t height);
  };
}

#{def struct background {
    struct surface base;
    struct window *window;
    struct widget *widget;
  };
}

#{def struct desktop {
    struct display *display;
    struct weston_desktop_shell *shell;
    struct output *output;
    struct window *grab_window;
    struct widget *grab_widget;
    enum cursor_type grab_cursor;
  };
}

#{def struct output {
    struct wl_output *output;
    struct background *background;
  };
}

newtype CursorType = CursorType { unCursorType :: CInt }
    deriving (Eq, Show)
#enum CursorType, CursorType, CURSOR_LEFT_PTR

data WlOutputInterface
foreign import ccall "&wl_output_interface"
    c_wl_output_interface :: Ptr WlOutputInterface
foreign import ccall "&weston_desktop_shell_interface"
    c_weston_desktop_shell_interface :: Ptr WlOutputInterface

data Display
data WestonDesktopShell
data Window
data Widget
data WlOutput
data WlSurface
data Input

data Surface = Surface (Ptr () -> Ptr WestonDesktopShell -> Word32 -> Ptr Window -> Int32 -> Int32 -> IO ())
instance Storable Surface where
    sizeOf _    = #{size struct surface}
    alignment _ = #{alignment struct surface}
    peek ptr = do
        c_funp <- #{peek struct surface, configure} ptr
        return (Surface (mkSurfaceConfigure c_funp))
    poke ptr (Surface c) = do
        c_funp <- mkSurfaceConfigureForeign c
        #{poke struct surface, configure} ptr c_funp
        --freeHaskellFunPtr c_funp

data Desktop = Desktop { desktopDisplay    :: Ptr Display
                       , desktopShell      :: Ptr WestonDesktopShell
                       , desktopOutput     :: Ptr Output
                       , desktopWindow     :: Ptr Window
                       , desktopWidget     :: Ptr Widget
                       , desktopCursorType :: CursorType
                       }
instance Storable Desktop where
    sizeOf _    = #{size struct desktop}
    alignment _ = #{alignment struct desktop}
    peek ptr = do
        d_ptr      <- #{peek struct desktop, display} ptr
        s_ptr      <- #{peek struct desktop, shell} ptr
        o_ptr      <- #{peek struct desktop, output} ptr
        window_ptr <- #{peek struct desktop, grab_window} ptr
        widget_ptr <- #{peek struct desktop, grab_widget} ptr
        c          <- #{peek struct desktop, grab_cursor} ptr
        return (Desktop d_ptr s_ptr o_ptr window_ptr widget_ptr (CursorType c))
    poke ptr (Desktop d_ptr s_ptr o_ptr window_ptr widget_ptr c) = do
        #{poke struct desktop, display} ptr d_ptr
        #{poke struct desktop, shell} ptr s_ptr
        #{poke struct desktop, output} ptr o_ptr
        #{poke struct desktop, grab_window} ptr window_ptr
        #{poke struct desktop, grab_widget} ptr widget_ptr
        #{poke struct desktop, grab_cursor} ptr (unCursorType c)

data Background = Background { backgroundSurface :: Surface
                             , backgroundWindow  :: Ptr Window
                             , backgroundWidget  :: Ptr Widget
                             }
instance Storable Background where
    sizeOf _    = #{size struct background}
    alignment _ = #{alignment struct background}
    peek ptr = do
        base       <- #{peek struct background, base} ptr
        window_ptr <- #{peek struct background, window} ptr
        widget_ptr <- #{peek struct background, widget} ptr
        return (Background base window_ptr widget_ptr)
    poke ptr (Background base window_ptr widget_ptr) = do
        #{poke struct background, base} ptr base
        #{poke struct background, window} ptr window_ptr
        #{poke struct background, widget} ptr widget_ptr

data Output = Output { outputWlOutput   :: Ptr WlOutput
                     , outputBackground :: Ptr Background
                     }
instance Storable Output where
    sizeOf _    = #{size struct output}
    alignment _ = #{alignment struct output}
    peek ptr = do
        o_ptr  <- #{peek struct output, output} ptr
        bg_ptr <- #{peek struct output, background} ptr
        return (Output o_ptr bg_ptr)
    poke ptr (Output o_ptr bg_ptr) = do
        #{poke struct output, output} ptr o_ptr
        #{poke struct output, background} ptr bg_ptr

data Listener = Listener (Ptr () -> Ptr WestonDesktopShell -> Word32 -> Ptr WlSurface -> Int32 -> Int32 -> IO ())
                         (Ptr () -> Ptr WestonDesktopShell -> IO ())
                         (Ptr () -> Ptr WestonDesktopShell -> CursorType -> IO ())
instance Storable Listener where
    sizeOf _    = #{size struct weston_desktop_shell_listener}
    alignment _ = #{alignment struct weston_desktop_shell_listener}
    peek ptr = do
        c_funp   <- #{peek struct weston_desktop_shell_listener, configure} ptr
        pls_funp <- #{peek struct weston_desktop_shell_listener, prepare_lock_surface} ptr
        gc_funp  <- #{peek struct weston_desktop_shell_listener, grab_cursor} ptr
        return (Listener (mkDesktopShellConfigure c_funp) (mkDesktopShellPrepareLockSurface pls_funp) (mkDesktopShellGrabCursor gc_funp))
    poke ptr (Listener c pls gc) = do
        c_funp <- mkDesktopShellConfigureForeign c
        pls_funp <- mkDesktopShellPrepareLockSurfaceForeign pls
        gc_funp <- mkDesktopShellGrabCursorForeign gc
        #{poke struct weston_desktop_shell_listener, configure} ptr c_funp
        #{poke struct weston_desktop_shell_listener, prepare_lock_surface} ptr pls_funp
        #{poke struct weston_desktop_shell_listener, grab_cursor} ptr gc_funp
        --freeHaskellFunPtr c_funp
        --freeHaskellFunPtr pls_funp
        --freeHaskellFunPtr gc_funp

foreign import ccall safe "widget_schedule_resize"
    c_widget_schedule_resize :: Ptr Widget -> Int32 -> Int32 -> IO ()

foreign import ccall safe "window_get_user_data"
    c_window_get_user_data :: Ptr Window -> IO (Ptr ())

foreign import ccall safe "wl_proxy_get_user_data"
    c_wl_surface_get_user_data :: Ptr WlSurface -> IO (Ptr ())

foreign import ccall safe "window_create_custom"
    c_window_create_custom :: Ptr Display -> IO (Ptr Window)

foreign import ccall safe "window_add_widget"
    c_window_add_widget :: Ptr Window -> Ptr () -> IO (Ptr Widget)

foreign import ccall safe "window_set_user_data"
    c_window_set_user_data :: Ptr Window -> Ptr () -> IO ()

foreign import ccall safe "widget_set_transparent"
    c_widget_set_transparent :: Ptr Widget -> CInt -> IO ()

foreign import ccall safe "window_get_wl_surface"
    c_window_get_wl_surface :: Ptr Window -> IO (Ptr WlSurface)

c_weston_desktop_shell_set_grab_surface :: Ptr WestonDesktopShell -> Ptr WlSurface -> IO ()
c_weston_desktop_shell_set_grab_surface ds_ptr s_ptr =
    c_wl_proxy_marshal ds_ptr #{const WESTON_DESKTOP_SHELL_SET_GRAB_SURFACE} (castPtr s_ptr :: Ptr ()) nullPtr

foreign import ccall safe "widget_set_allocation"
    c_widget_set_allocation :: Ptr Widget -> CInt -> CInt -> CInt -> CInt -> IO ()

foreign import ccall safe "widget_set_enter_handler"
    c_widget_set_enter_handler :: Ptr Widget -> FunPtr (Ptr Widget -> Ptr Input -> Float -> Float -> Ptr () -> IO (CursorType)) -> IO ()

foreign import ccall safe "wl_proxy_marshal"
    c_wl_proxy_marshal :: Ptr WestonDesktopShell -> CInt -> Ptr () -> Ptr () -> IO ()

c_weston_desktop_shell_set_background :: Ptr WestonDesktopShell -> Ptr WlOutput -> Ptr WlSurface -> IO ()
c_weston_desktop_shell_set_background ds_ptr wlo_ptr s_ptr =
    c_wl_proxy_marshal ds_ptr #{const WESTON_DESKTOP_SHELL_SET_BACKGROUND} (castPtr wlo_ptr :: Ptr ()) (castPtr s_ptr :: Ptr ())

foreign import ccall safe "display_bind"
    c_display_bind :: Ptr Display -> Word32 -> Ptr WlOutputInterface -> CInt -> IO (Ptr WlOutput)

foreign import ccall safe "wl_proxy_add_listener"
    c_weston_desktop_shell_add_listener :: Ptr WestonDesktopShell -> Ptr Listener -> Ptr Desktop -> IO ()

c_weston_desktop_shell_desktop_ready :: Ptr WestonDesktopShell -> IO ()
c_weston_desktop_shell_desktop_ready ds_ptr =
    c_wl_proxy_marshal ds_ptr #{const WESTON_DESKTOP_SHELL_DESKTOP_READY} nullPtr nullPtr

foreign import ccall safe "display_create"
    c_display_create :: CInt -> Ptr CString -> IO (Ptr Display)

foreign import ccall safe "display_run"
    c_display_run :: Ptr Display -> IO ()

foreign import ccall safe "display_set_user_data"
    c_display_set_user_data :: Ptr Display -> Ptr () -> IO ()

foreign import ccall safe "display_set_global_handler"
    c_display_set_global_handler :: Ptr Display -> FunPtr (Ptr Display -> Word32 -> CString -> Word32 -> Ptr () -> IO ()) -> IO ()

foreign import ccall "wrapper"
    mkSurfaceConfigureForeign ::            (Ptr () -> Ptr WestonDesktopShell -> Word32 -> Ptr Window -> Int32 -> Int32 -> IO ()) ->
                                 IO (FunPtr (Ptr () -> Ptr WestonDesktopShell -> Word32 -> Ptr Window -> Int32 -> Int32 -> IO ()))

foreign import ccall "wrapper"
    mkDesktopShellConfigureForeign ::            (Ptr () -> Ptr WestonDesktopShell -> Word32 -> Ptr WlSurface -> Int32 -> Int32 -> IO ()) ->
                                      IO (FunPtr (Ptr () -> Ptr WestonDesktopShell -> Word32 -> Ptr WlSurface -> Int32 -> Int32 -> IO ()))

foreign import ccall "wrapper"
    mkDesktopShellPrepareLockSurfaceForeign ::            (Ptr () -> Ptr WestonDesktopShell -> IO ()) ->
                                               IO (FunPtr (Ptr () -> Ptr WestonDesktopShell -> IO ()))

foreign import ccall "wrapper"
    mkDesktopShellGrabCursorForeign ::            (Ptr () -> Ptr WestonDesktopShell -> CursorType -> IO ()) ->
                                       IO (FunPtr (Ptr () -> Ptr WestonDesktopShell -> CursorType -> IO ()))

foreign import ccall "wrapper"
    mkGrabSurfaceEnterHandlerForeign ::            (Ptr Widget -> Ptr Input -> Float -> Float -> Ptr () -> IO (CursorType)) ->
                                        IO (FunPtr (Ptr Widget -> Ptr Input -> Float -> Float -> Ptr () -> IO (CursorType)))

foreign import ccall "wrapper"
    mkGlobalHandlerForeign ::            (Ptr Display -> Word32 -> CString -> Word32 -> Ptr () -> IO ()) ->
                              IO (FunPtr (Ptr Display -> Word32 -> CString -> Word32 -> Ptr () -> IO ()))

foreign import ccall "dynamic"
    mkSurfaceConfigure :: FunPtr (Ptr () -> Ptr WestonDesktopShell -> Word32 -> Ptr Window -> Int32 -> Int32 -> IO ()) ->
                                 (Ptr () -> Ptr WestonDesktopShell -> Word32 -> Ptr Window -> Int32 -> Int32 -> IO ())

foreign import ccall "dynamic"
    mkDesktopShellConfigure :: FunPtr (Ptr () -> Ptr WestonDesktopShell -> Word32 -> Ptr WlSurface -> Int32 -> Int32 -> IO ()) ->
                                      (Ptr () -> Ptr WestonDesktopShell -> Word32 -> Ptr WlSurface -> Int32 -> Int32 -> IO ())

foreign import ccall "dynamic"
    mkDesktopShellPrepareLockSurface :: FunPtr (Ptr () -> Ptr WestonDesktopShell -> IO ()) ->
                                               (Ptr () -> Ptr WestonDesktopShell -> IO ())

foreign import ccall "dynamic"
    mkDesktopShellGrabCursor :: FunPtr (Ptr () -> Ptr WestonDesktopShell -> CursorType -> IO ()) ->
                                       (Ptr () -> Ptr WestonDesktopShell -> CursorType -> IO ())

backgroundConfigure d_ptr ds_ptr edges window_ptr w h = do
    bg_ptr <- c_window_get_user_data window_ptr >>= return . castPtr :: IO (Ptr Background)
    widget_ptr <- peek bg_ptr >>= return . backgroundWidget
    c_widget_schedule_resize widget_ptr w h

desktopShellConfigure d_ptr ds_ptr edges wl_surface_ptr w h = do
    window_ptr <- c_wl_surface_get_user_data wl_surface_ptr >>= return . castPtr :: IO (Ptr Window)
    surface_ptr <- c_window_get_user_data window_ptr >>= return . castPtr :: IO (Ptr Surface)
    Surface configure <- peek surface_ptr
    configure d_ptr ds_ptr edges window_ptr w h

desktopShellPrepareLockSurface d_ptr ds_ptr = return ()

desktopShellGrabCursor d_ptr ds_ptr c = do
    let desktop_ptr = castPtr d_ptr :: Ptr Desktop
    peek desktop_ptr >>= \desktop -> poke desktop_ptr desktop { desktopCursorType = c }

backgroundCreate desktop_ptr = do
    bg_fp <- mallocForeignPtr :: IO (ForeignPtr Background)
    withForeignPtr bg_fp $ \bg_ptr -> do
        display_ptr <- peek desktop_ptr >>= return . desktopDisplay
        let base = Surface backgroundConfigure
        window_ptr <- c_window_create_custom display_ptr
        widget_ptr <- c_window_add_widget window_ptr (castPtr bg_ptr :: Ptr ())
        poke bg_ptr (Background base window_ptr widget_ptr)
        c_window_set_user_data window_ptr (castPtr bg_ptr :: Ptr ())
        c_widget_set_transparent widget_ptr 0
    return bg_fp

grabSurfaceEnterHandler widget_ptr input_ptr x y d_ptr = do
    let desktop_ptr = castPtr d_ptr :: Ptr Desktop
    peek desktop_ptr >>= return . desktopCursorType

grabSurfaceCreate desktop_ptr = do
    Desktop display_ptr ds_ptr _ _ _ gc <- peek desktop_ptr
    window_ptr <- c_window_create_custom display_ptr
    c_window_set_user_data window_ptr (castPtr desktop_ptr :: Ptr ())
    s <- c_window_get_wl_surface window_ptr
    c_weston_desktop_shell_set_grab_surface ds_ptr s
    widget_ptr <- c_window_add_widget window_ptr (castPtr desktop_ptr :: Ptr ())
    c_widget_set_allocation widget_ptr 0 0 1 1
    gseh_funp <- mkGrabSurfaceEnterHandlerForeign grabSurfaceEnterHandler
    c_widget_set_enter_handler widget_ptr gseh_funp
    --freeHaskellFunPtr gseh_funp
    peek desktop_ptr >>= \desktop -> poke desktop_ptr desktop { desktopWindow = window_ptr, desktopWidget = widget_ptr }

outputInit o_ptr desktop_ptr = do
    ds_ptr <- peek desktop_ptr >>= return . desktopShell
    wlo_ptr <- peek o_ptr >>= return . outputWlOutput
    bg_fp <- backgroundCreate desktop_ptr
    withForeignPtr bg_fp $ \bg_ptr -> do
        window_ptr <- peek bg_ptr >>= return . backgroundWindow
        s <- c_window_get_wl_surface window_ptr
        c_weston_desktop_shell_set_background ds_ptr wlo_ptr s

createOutput desktop_ptr id = do
    Desktop display_ptr ds_ptr _ _ _ gc <- peek desktop_ptr
    wlo_ptr <- c_display_bind display_ptr id c_wl_output_interface 2
    o_fp <- mallocForeignPtr :: IO (ForeignPtr Output)
    withForeignPtr o_fp $ \o_ptr -> do
        peek o_ptr >>= \o -> poke o_ptr o { outputWlOutput = wlo_ptr }
        peek desktop_ptr >>= \desktop -> poke desktop_ptr desktop { desktopOutput = o_ptr }
        if ds_ptr /= nullPtr
            then outputInit o_ptr desktop_ptr
            else return ()

globalHandler _ id interface_cs version d_ptr = do
    with (Listener desktopShellConfigure desktopShellPrepareLockSurface desktopShellGrabCursor) $ \l_ptr -> do
        let desktop_ptr = castPtr d_ptr :: Ptr Desktop
        display_ptr <- peek desktop_ptr >>= return . desktopDisplay
        interface <- peekCString interface_cs -- Where is this freed?
        if interface == "weston_desktop_shell"
            then do ds_ptr <- c_display_bind display_ptr id c_weston_desktop_shell_interface 1 >>= return . castPtr :: IO (Ptr WestonDesktopShell)
                    peek desktop_ptr >>= \desktop -> poke desktop_ptr desktop { desktopShell = ds_ptr }
                    c_weston_desktop_shell_add_listener ds_ptr l_ptr desktop_ptr
                    c_weston_desktop_shell_desktop_ready ds_ptr
            else if interface == "wl_output"
            then createOutput desktop_ptr id
            else return ()

displayCreate = alloca $ \argv -> c_display_create 0 argv >>= newForeignPtr finalizerFree

main = do
    desktop_fp <- mallocForeignPtr :: IO (ForeignPtr Desktop)
    display_fp <- displayCreate
    withForeignPtr display_fp $ \display_ptr -> do
    withForeignPtr desktop_fp $ \desktop_ptr -> do
        peek desktop_ptr >>= \desktop -> poke desktop_ptr desktop { desktopDisplay = display_ptr }
        gh_funp <- mkGlobalHandlerForeign globalHandler
        c_display_set_user_data display_ptr (castPtr desktop_ptr :: Ptr ())
        c_display_set_global_handler display_ptr gh_funp
        o_ptr <- peek desktop_ptr >>= return . desktopOutput
        bg_ptr <- peek o_ptr >>= return . outputBackground
        if bg_ptr == nullPtr
            then outputInit o_ptr desktop_ptr
            else return ()
        grabSurfaceCreate desktop_ptr
        c_display_run display_ptr
        -- Clean up
        freeHaskellFunPtr gh_funp
    return 0

