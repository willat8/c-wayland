/*
 * Copyright © 2011 Kristian Høgsberg
 * Copyright © 2011 Collabora, Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <cairo.h>
#include <sys/time.h>
#include <sys/timerfd.h>
#include <sys/epoll.h>
#include <unistd.h>

#include <linux/input.h>
#include <wayland-client.h>
#include "window.h"
#include "shared/helpers.h"
#include "shared/xalloc.h"

#include "weston-desktop-shell-client-protocol.h"

struct desktop {
	struct display *display;
	struct weston_desktop_shell *shell;
	struct wl_list outputs;

	struct window *grab_window;
	struct widget *grab_widget;

	enum cursor_type grab_cursor;
};

struct surface {
	void (*configure)(void *data,
			  struct weston_desktop_shell *desktop_shell,
			  uint32_t edges, struct window *window,
			  int32_t width, int32_t height);
};

struct background {
	struct surface base;
	struct window *window;
	struct widget *widget;
};

struct output {
	struct wl_output *output;
	uint32_t server_output_id;
	struct wl_list link;

	struct background *background;
};

struct status {
	struct display *display;
	struct window *window;
	struct widget *widget;
	int width, height;
	int check_fd;
	struct task check_task;
};

static void
draw_square(cairo_t *cr, int is_green, int width, double x, double y)
{
	int height = width;
	double aspect        = 1.0,           /* aspect ratio */
		   corner_radius = height / 10.0; /* and corner curvature radius */

	double radius = corner_radius / aspect;
	double degrees = M_PI / 180.0;

	cairo_new_sub_path (cr);
	cairo_arc (cr, x + width - radius, y + radius, radius, -90 * degrees, 0 * degrees);
	cairo_arc (cr, x + width - radius, y + height - radius, radius, 0 * degrees, 90 * degrees);
	cairo_arc (cr, x + radius, y + height - radius, radius, 90 * degrees, 180 * degrees);
	cairo_arc (cr, x + radius, y + radius, radius, 180 * degrees, 270 * degrees);
	cairo_close_path (cr);

	if (is_green)
		cairo_set_source_rgb (cr, 148/256.0, 194/256.0, 105/256.0);
	else
		cairo_set_source_rgb (cr, 239/256.0, 41/256.0, 41/256.0);
	cairo_fill_preserve (cr);
	if (is_green)
		cairo_set_source_rgba (cr, 148/256.0, 194/256.0, 105/256.0, 0.5);
	else
		cairo_set_source_rgba (cr, 239/256.0, 41/256.0, 41/256.0, 0.5);
	cairo_set_line_width (cr, 10.0);
	cairo_stroke (cr);
}

static void
draw_status(cairo_surface_t *surface, int width, int height)
{
	int sq_dim, y, init_x;
	cairo_t *cr;

	cr = cairo_create(surface);
	cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
	cairo_set_source_rgba(cr, 0, 0, 0, 0);
	cairo_paint(cr);

	cairo_set_operator(cr, CAIRO_OPERATOR_OVER);

	sq_dim = 100;
	y = (height - sq_dim) / 2;
	init_x = 160;
	draw_square(cr, random() < RAND_MAX / 2, sq_dim, init_x, y);
	draw_square(cr, random() < RAND_MAX / 2, sq_dim, (width - sq_dim) / 2, y);
	draw_square(cr, random() < RAND_MAX / 2, sq_dim, (width - init_x) - sq_dim, y);

	cairo_destroy(cr);
}

static void
status_check_func(struct task *task, uint32_t events)
{
	struct status *status = container_of(task, struct status, check_task);
	uint64_t exp;

	if (read(status->check_fd, &exp, sizeof exp) != sizeof exp)
		abort();

	widget_schedule_redraw(status->widget);
}

static void
frame_callback(void *data, struct wl_callback *callback, uint32_t time)
{
	struct status *status = data;

	window_schedule_redraw(status->window);

	if (callback)
		wl_callback_destroy(callback);
}

static const struct wl_callback_listener callback_listener = {
	frame_callback
};

static void
resize_handler(struct widget *widget,
	       int32_t width, int32_t height, void *data)
{
	struct status *status = data;

	/* Don't resize me */
	widget_set_size(status->widget, status->width, status->height);
}

static void
redraw_handler(struct widget *widget, void *data)
{
	struct status *status = data;
	cairo_surface_t *surface;
	//struct wl_callback *callback;

	surface = window_get_surface(status->window);
	if (surface == NULL ||
	    cairo_surface_status(surface) != CAIRO_STATUS_SUCCESS) {
		fprintf(stderr, "failed to create cairo egl surface\n");
		return;
	}

	draw_status(surface, status->width, status->height);
	cairo_surface_destroy(surface);

	//callback = wl_surface_frame(window_get_wl_surface(status->window));
	//wl_callback_add_listener(callback, &callback_listener, status);
}


static void
button_handler(struct widget *widget,
	       struct input *input, uint32_t time,
	       uint32_t button, enum wl_pointer_button_state state, void *data)
{
	struct status *status = data;

	if (state == WL_POINTER_BUTTON_STATE_PRESSED)
		window_move(status->window, input,
				display_get_serial(status->display));
}

static void
touch_down_handler(struct widget *widget, struct input *input,
		   uint32_t serial, uint32_t time, int32_t id,
		   float x, float y, void *data)
{
	struct status *status = data;
	window_move(status->window, input, display_get_serial(status->display));
}

static void
background_configure(void *data,
		     struct weston_desktop_shell *desktop_shell,
		     uint32_t edges, struct window *window,
		     int32_t width, int32_t height)
{
	struct background *background =
		(struct background *) window_get_user_data(window);

	widget_schedule_resize(background->widget, width, height);
}

static void
desktop_shell_configure(void *data,
			struct weston_desktop_shell *desktop_shell,
			uint32_t edges,
			struct wl_surface *surface,
			int32_t width, int32_t height)
{
	struct window *window = wl_surface_get_user_data(surface);
	struct surface *s = window_get_user_data(window);

	s->configure(data, desktop_shell, edges, window, width, height);
}

static void
desktop_shell_prepare_lock_surface(void *data,
				   struct weston_desktop_shell *desktop_shell)
{
}

static void
desktop_shell_grab_cursor(void *data,
			  struct weston_desktop_shell *desktop_shell,
			  uint32_t cursor)
{
	struct desktop *desktop = data;
	desktop->grab_cursor = CURSOR_LEFT_PTR;
}

static const struct weston_desktop_shell_listener listener = {
	desktop_shell_configure,
	desktop_shell_prepare_lock_surface,
	desktop_shell_grab_cursor
};

static void
background_destroy(struct background *background)
{
	widget_destroy(background->widget);
	window_destroy(background->window);

	free(background);
}

static struct background *
background_create(struct desktop *desktop)
{
	struct background *background;

	background = xzalloc(sizeof *background);
	background->base.configure = background_configure;
	background->window = window_create_custom(desktop->display);
	background->widget = window_add_widget(background->window, background);
	window_set_user_data(background->window, background);
	widget_set_transparent(background->widget, 0);

	return background;
}

static int
grab_surface_enter_handler(struct widget *widget, struct input *input,
			   float x, float y, void *data)
{
	struct desktop *desktop = data;

	return desktop->grab_cursor;
}

static void
grab_surface_destroy(struct desktop *desktop)
{
	widget_destroy(desktop->grab_widget);
	window_destroy(desktop->grab_window);
}

static void
grab_surface_create(struct desktop *desktop)
{
	struct wl_surface *s;

	desktop->grab_window = window_create_custom(desktop->display);
	window_set_user_data(desktop->grab_window, desktop);

	s = window_get_wl_surface(desktop->grab_window);
	weston_desktop_shell_set_grab_surface(desktop->shell, s);

	desktop->grab_widget =
		window_add_widget(desktop->grab_window, desktop);
	/* We set the allocation to 1x1 at 0,0 so the fake enter event
	 * at 0,0 will go to this widget. */
	widget_set_allocation(desktop->grab_widget, 0, 0, 1, 1);

	widget_set_enter_handler(desktop->grab_widget,
				 grab_surface_enter_handler);
}

static void
output_destroy(struct output *output)
{
	background_destroy(output->background);
	wl_output_destroy(output->output);
	wl_list_remove(&output->link);

	free(output);
}

static void
desktop_destroy_outputs(struct desktop *desktop)
{
	struct output *tmp;
	struct output *output;

	wl_list_for_each_safe(output, tmp, &desktop->outputs, link)
		output_destroy(output);
}

static void
output_init(struct output *output, struct desktop *desktop)
{
	struct wl_surface *surface;

	output->background = background_create(desktop);
	surface = window_get_wl_surface(output->background->window);
	weston_desktop_shell_set_background(desktop->shell,
					    output->output, surface);
}

static void
create_output(struct desktop *desktop, uint32_t id)
{
	struct output *output;

	output = zalloc(sizeof *output);
	if (!output)
		return;

	output->output =
		display_bind(desktop->display, id, &wl_output_interface, 2);
	output->server_output_id = id;

	wl_list_insert(&desktop->outputs, &output->link);

	/* On start up we may process an output global before the shell global
	 * in which case we can't create the panel and background just yet */
	if (desktop->shell)
		output_init(output, desktop);
}

static void
global_handler(struct display *display, uint32_t id,
	       const char *interface, uint32_t version, void *data)
{
	struct desktop *desktop = data;

	if (!strcmp(interface, "weston_desktop_shell")) {
		desktop->shell = display_bind(desktop->display,
					      id,
					      &weston_desktop_shell_interface,
					      1);
		weston_desktop_shell_add_listener(desktop->shell,
						  &listener,
						  desktop);
		weston_desktop_shell_desktop_ready(desktop->shell);
	} else if (!strcmp(interface, "wl_output")) {
		create_output(desktop, id);
	}
}

static void
global_handler_remove(struct display *display, uint32_t id,
	       const char *interface, uint32_t version, void *data)
{
	struct desktop *desktop = data;
	struct output *output;

	if (!strcmp(interface, "wl_output")) {
		wl_list_for_each(output, &desktop->outputs, link) {
			if (output->server_output_id == id) {
				output_destroy(output);
				break;
			}
		}
	}
}

int main(int argc, char *argv[])
{
	struct desktop desktop = { 0 };
	struct output *output;
	struct status status;
	struct timeval tv;
	struct itimerspec its;

	wl_list_init(&desktop.outputs);

	desktop.display = display_create(&argc, argv);
	if (desktop.display == NULL) {
		fprintf(stderr, "failed to create display: %m\n");
		return -1;
	}

	display_set_user_data(desktop.display, &desktop);
	display_set_global_handler(desktop.display, global_handler);
	display_set_global_handler_remove(desktop.display, global_handler_remove);

	/* Create panel and background for outputs processed before the shell
	 * global interface was processed */
	wl_list_for_each(output, &desktop.outputs, link)
		if (!output->background)
			output_init(output, &desktop);

	grab_surface_create(&desktop);

	gettimeofday(&tv, NULL);
	srandom(tv.tv_usec);

	status.width = 800;
	status.height = 480;
	status.display = desktop.display;
	status.window = window_create(desktop.display);
	status.widget = window_add_widget(status.window, &status);
	status.check_fd = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC);
	status.check_task.run = status_check_func;
	display_watch_fd(desktop.display, status.check_fd, EPOLLIN, &status.check_task);

	its.it_interval.tv_sec = 5;
	its.it_interval.tv_nsec = 0;
	its.it_value.tv_sec = 5;
	its.it_value.tv_nsec = 0;
	timerfd_settime(status.check_fd, 0, &its, NULL);
	
	widget_set_resize_handler(status.widget, resize_handler);
	widget_set_redraw_handler(status.widget, redraw_handler);
	widget_set_button_handler(status.widget, button_handler);
	widget_set_touch_down_handler(status.widget, touch_down_handler);
	window_schedule_resize(status.window, status.width, status.height);

	display_run(desktop.display);

	/* Cleanup */
	display_unwatch_fd(desktop.display, status.check_fd);
	close(status.check_fd);
	widget_destroy(status.widget);
	window_destroy(status.window);
	grab_surface_destroy(&desktop);
	desktop_destroy_outputs(&desktop);
	weston_desktop_shell_destroy(desktop.shell);
	display_destroy(desktop.display);

	return 0;
}

