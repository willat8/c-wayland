{-# LINE 1 "hswayland.hsc" #-}
{-# LANGUAGE ForeignFunctionInterface, EmptyDataDecls #-}
{-# LINE 2 "hswayland.hsc" #-}

import Foreign
import Foreign.Ptr
import Foreign.C.String
import Foreign.C.Types
import System.IO.Unsafe


{-# LINE 10 "hswayland.hsc" #-}


{-# LINE 12 "hswayland.hsc" #-}

data Display
data Window
data Widget
data Container = Container { display :: ForeignPtr Display, window :: ForeignPtr Window }

instance Storable Container where
    sizeOf _    = (16)
{-# LINE 20 "hswayland.hsc" #-}
    alignment _ = (8)
{-# LINE 21 "hswayland.hsc" #-}
    peek ptr = do
        d_ptr <- (\hsc_ptr -> peekByteOff hsc_ptr 0) ptr
{-# LINE 23 "hswayland.hsc" #-}
        w_ptr <- (\hsc_ptr -> peekByteOff hsc_ptr 8) ptr
{-# LINE 24 "hswayland.hsc" #-}
        d_fp <- newForeignPtr finalizerFree d_ptr
        w_fp <- newForeignPtr finalizerFree w_ptr
        return (Container d_fp w_fp)
    poke ptr (Container d_fp w_fp) = do
        withForeignPtr d_fp $ \d_ptr -> do
        withForeignPtr w_fp $ \w_ptr -> do
            (\hsc_ptr -> pokeByteOff hsc_ptr 0) ptr d_ptr
{-# LINE 31 "hswayland.hsc" #-}
            (\hsc_ptr -> pokeByteOff hsc_ptr 8) ptr w_ptr
{-# LINE 32 "hswayland.hsc" #-}

foreign import ccall unsafe "display_create"
    c_display_create :: CInt -> Ptr CString -> IO (Ptr Display)

display_create = unsafePerformIO $
    alloca $ \argv -> do
        d_ptr <- c_display_create 0 argv
        d_fp  <- newForeignPtr finalizerFree d_ptr
        return d_fp

foreign import ccall unsafe "window_create"
    c_window_create :: Ptr Display -> IO (Ptr Window)

window_create d_fp = unsafePerformIO $ do
    withForeignPtr d_fp $ \d_ptr -> do
        w_ptr <- c_window_create d_ptr
        w_fp <- newForeignPtr finalizerFree w_ptr
        return w_fp

foreign import ccall unsafe "window_frame_create"
    c_window_frame_create :: Ptr Window -> Ptr a -> IO (Ptr Widget)

window_frame_create w_fp c_ptr = unsafePerformIO $ do
    withForeignPtr w_fp $ \w_ptr -> do
        wdg_ptr <- c_window_frame_create w_ptr c_ptr
        wdg_fp <- newForeignPtr finalizerFree wdg_ptr
        return wdg_fp

foreign import ccall unsafe "display_run"
    c_display_run :: Ptr Display -> IO ()

display_run d_fp = unsafePerformIO $ do
    withForeignPtr d_fp $ \d_ptr -> do
        return $ c_display_run d_ptr

foreign import ccall unsafe "widget_schedule_resize"
    c_widget_schedule_resize :: Ptr Widget -> CInt -> CInt -> IO ()

widget_schedule_resize wdg_fp = unsafePerformIO $ do
    withForeignPtr wdg_fp $ \wdg_ptr -> do
        return $ c_widget_schedule_resize wdg_ptr 200 200

main = do
    let d_fp = display_create
    let w_fp = window_create d_fp
    alloca $ \c_ptr -> do
        let c = Container d_fp w_fp
        poke c_ptr c
        let wdg = window_frame_create w_fp c_ptr
        widget_schedule_resize wdg
    display_run d_fp
    return 0

